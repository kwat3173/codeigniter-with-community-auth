# README #

I have seen on the codeigniter forums that alot of people are having trouble installing Community Auth into Codeigniter 3. 
So I created this Repository to make installing it even easier. 

For more information on Community Auth please visit there website at https://community-auth.com/. 
The software belongs to Robert B Gottier.

### How to install this respository. ###

1) Download and extract the files to your hard drive.

2) Open the repository folder that you just extracted and select everything then copy it to your codeigniter path.

3) use phpadmin to create a database and import the database file located in the sql folder.

4) Open config/database.php and edit the database details to match your new database.

5) open config/config.php and edit the base_url with the path to your codeigniter installation


6) now go to http://your_codeigniter_directory/key_creator and select the codeigniter standard 128bit encryption, this should be the very first one.

7) after you selected a new window will open showing you the encryption key select the text and copy it

8) open config/config.php and paste that code into the encryption key. I normally just paste it and use // in front of the old entry to comment it out.

9) open controllers/Examples and edit lines 195,196, and 197 with your new admin login details then change line 198 from a 1 to 9.

10) now go to http://your_codeigniter_directory/examples/create_user once completed you will see a message showing that the user has been created

11) now try login in by going to http://your_codeigniter_directory/example. Once logged in you will see a message showing that your logged in

12) to logout got to http://your_codeigniter_directory/example/logout

Happy Coding and visit the official Community Auth website for online documentation!